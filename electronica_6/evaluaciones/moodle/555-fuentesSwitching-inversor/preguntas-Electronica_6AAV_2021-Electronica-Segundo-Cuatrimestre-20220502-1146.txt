// question: 0  name: Switch category to $course$/top/Electronica-Segundo-Cuatrimestre
$CATEGORY: $course$/top/Electronica-Segundo-Cuatrimestre


// question: 9364  name: 555
::555::[html]<p>Determinar cual de las siguientes opciones es la correcta</p><p><img src\="@@PLUGINFILE@@/20210913_555-cto.jpg" alt\="" role\="presentation"><br></p>{
	~<p>la oscilación esta dada por los pines 4 y 8</p>
	=<p>la oscilación esta dada por valores de RA, RB Y C1</p>
	~<p>la oscilación esta dada por valores de RA, RB , C1 y C2<br></p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones es correcta</p>
}


// question: 9365  name: 555
::555::[html]<p>Determinar cual de las siguientes opciones es la correcta</p><p><img src\="@@PLUGINFILE@@/20210913_555-cto.jpg" alt\="" role\="presentation"><br></p>{
	~<p>la oscilación esta dada por los pines 4 y 8</p>
	~<p>la oscilación esta dada por valores de RA, C1 Y C2</p>
	~<p>la oscilación esta dada por valores de RA, RB , C1 y C2<br></p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	=<p>ninguna de las respuestas con afirmaciones es correcta</p>
}


// question: 9366  name: 555
::555::[html]<p>Determinar cual de las siguientes opciones es la correcta</p><p><img src\="@@PLUGINFILE@@/20210913_555-cto.jpg" alt\="" role\="presentation"><br></p>{
	~<p>El pin 4 corresponde al RESET y el pin 8 a la alimentación Vcc</p>
	~<p>la oscilación esta dada por valores de RA, RB Y C1</p>
	~<p>el 555 en su configuración de astable genera una onda cuadrada<br></p>
	=<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones es correcta</p>
}


// question: 9367  name: 555-onda
::555-onda::[html]<p>Seleccionar la respuesta correcta.</p><p><img src\="@@PLUGINFILE@@/20210913_555-onda-th-tl-bien.jpg" alt\="" role\="presentation"><br></p>{
	~<p>TH \= C1.(RA+RB).ln(2)</p>
	~<p>TL \= C1.RB.ln(2)<br></p>
	~<p>T\=TH+TL</p>
	=<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones son correctas</p>
}


// question: 9368  name: 555-onda
::555-onda::[html]<p>Seleccionar la respuesta correcta.</p><p><img src\="@@PLUGINFILE@@/20210913_555-onda-th-tl-bien.jpg" alt\="" role\="presentation"><br></p>{
	~<p>TL \= C1.(RA+RB).ln(2)</p>
	~<p>TH \= C1.RB.ln(2)<br></p>
	~<p>El cto integrado 555 en su modo astable genera una onda senoidal</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	=<p>ninguna de las respuestas con afirmaciones son correctas</p>
}


// question: 9369  name: 555-onda
::555-onda::[html]<p>Seleccionar la respuesta correcta.</p><p><img src\="@@PLUGINFILE@@/20210913_555-onda-th-tl-bien.jpg" alt\="" role\="presentation"><br></p>{
	=<p>TH \= C1.(RA+RB).ln(2)</p>
	~<p>TH \= C1.RB.ln(2)<br></p>
	~<p>El cto integrado 555 en su modo astable genera una onda senoidal</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones son correctas</p>
}


// question: 9370  name: 555-onda
::555-onda::[html]<p>Seleccionar la respuesta correcta.</p><p><img src\="@@PLUGINFILE@@/20210913_555-onda-th-tl-bien.jpg" alt\="" role\="presentation"><br></p>{
	~<p>TL \= C1.(RA+RB).ln(2)</p>
	=<p>TL \= C1.RB.ln(2)<br></p>
	~<p>El cto integrado 555 en su modo astable genera una onda senoidal</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones son correctas</p>
}


// question: 9349  name: fuente - Switching a completar
::fuente - Switching a completar::[html]<p>indicar cual de las siguientes afirmaciones es la correcta, con relación a los módulos indicados con los números</p><p><img src\="@@PLUGINFILE@@/20210913_fuenteSwitching-acompletar.jpg" alt\="" role\="presentation"><br></p>{
	~1-&nbsp;Conmutador (oscilador 100khz),&nbsp;
	~<p>2-&nbsp;Rectificador 220V AC-DC,&nbsp;</p><p>2-Conmutador (oscilador 100khz</p>
	=<p>3-Transformador 220V-12V CA<br></p>
	~<p>Todas las afirmaciones son correctas</p>
	~<p>Ninguna de las afirmaciones es correcta</p>
}


// question: 9350  name: fuente - Switching a completar
::fuente - Switching a completar::[html]<p>indicar cual de las siguientes afirmaciones es la correcta, con relación a los módulos indicados con los números</p><p><img src\="@@PLUGINFILE@@/20210913_fuenteSwitching-acompletar.jpg" alt\="" role\="presentation"><br></p>{
	~1-&nbsp;Rectificador 220V AC-DC
	~<p>2-Conmutador (oscilador 100khz)</p><p>2-Conmutador (oscilador 100khz</p>
	~<p>3-Transformador 220V-12V CA<br></p>
	~<p>4-Rectificador 12V AC-DC</p>
	=<p>Todas las afirmaciones son correcta</p>
}


// question: 9351  name: fuente - Switching a completar
::fuente - Switching a completar::[html]<p>indicar cual de las siguientes afirmaciones es la correcta, con relación a los módulos indicados con los números</p><p><img src\="@@PLUGINFILE@@/20210913_fuenteSwitching-acompletar.jpg" alt\="" role\="presentation"><br></p>{
	~1-&nbsp;Conmutador (oscilador 100khz)
	~<p>2-Rectificador 220V AC-DC&nbsp;</p><p>2-Conmutador (oscilador 100khz</p>
	~<p>3-Rectificador 12V AC-DC<br></p>
	=<p>Ninguna de las afirmaciones es correcta</p>
	~<p>Todas las afirmaciones son correcta</p>
}


// question: 9352  name: fuente - Switching a completar
::fuente - Switching a completar::[html]<p>indicar cual de las siguientes afirmaciones es la correcta, con relación a los módulos indicados con los números</p><p><img src\="@@PLUGINFILE@@/20210913_fuenteSwitching-acompletar.jpg" alt\="" role\="presentation"><br></p>{
	~1-&nbsp;Conmutador (oscilador 100khz)
	=<p>1-Rectificador 220V AC-DC&nbsp;</p><p>2-Conmutador (oscilador 100khz</p>
	~<p>1-Transformador 220V-12V CA<br></p>
	=<p>Ninguna de las afirmaciones es correcta</p>
	~<p>Todas las afirmaciones son correcta</p>
}


// question: 9353  name: fuente - Switching a completar
::fuente - Switching a completar::[html]<p>indicar cual de las siguientes afirmaciones es la correcta, con relación a los módulos indicados con los números</p><p><img src\="@@PLUGINFILE@@/20210913_fuenteSwitching-acompletar.jpg" alt\="" role\="presentation"><br></p>{
	=2-Conmutador (oscilador 100khz)
	~<p>2-Rectificador 220V AC-DC&nbsp;</p><p>2-Conmutador (oscilador 100khz</p>
	~<p>1-Transformador 220V-12V CA<br></p>
	~<p>Ninguna de las afirmaciones es correcta</p>
	~<p>Todas las afirmaciones son correcta</p>
}


// question: 9354  name: fuente - Switching a completar.
::fuente - Switching a completar.::[html]<p>indicar cual de las siguientes afirmaciones es la correcta, con relación a los módulos indicados con los números</p><p><img src\="@@PLUGINFILE@@/20210913_fuenteSwitching-acompletar.jpg" alt\="" role\="presentation"><br></p>{
	~1-Conmutador (oscilador 100khz)
	~<p>2-Rectificador 220V AC-DC&nbsp;</p><p>2-Conmutador (oscilador 100khz</p>
	=<p>4-Rectificador 12V AC-DC<br></p>
	~<p>Ninguna de las afirmaciones es correcta</p>
	~<p>Todas las afirmaciones son correcta</p>
}


// question: 9355  name: fuente switching a insertar
::fuente switching a insertar::[html]<p>Debe indicar el elemento faltante para completar la fuente</p><p><img src\="@@PLUGINFILE@@/diagramaEnBloques5-fala%20trafo.png" alt\="" role\="presentation"><br></p><p><br></p>{
	~<p>ente 1 y 2 Conmutador (oscilador 100khz)<br></p>
	~<p>ente 1 y 2 Transformador 220V-12V CA<br></p>
	~<p>todas las opciones con afirmaciones son correctas</p>
	=<p>ninguna de las afirmaciones es correcta</p>
	~<p>ente 2 y 3 Conmutador (oscilador 100khz)<br></p>
}


// question: 9356  name: fuente switching a insertar
::fuente switching a insertar::[html]<p>Debe indicar el elemento faltante para completar la fuente</p><p><img src\="@@PLUGINFILE@@/diagramaEnBloques5-fala%20trafo.png" alt\="" role\="presentation"><br></p><p><br></p>{
	~<p>ente 2 y 3 Conmutador (oscilador 100khz)<br></p>
	=<p>ente 2 y 3 Transformador 220V-12V CA<br></p>
	~<p>todas las opciones con afirmaciones son correctas</p>
	~<p>ninguna de las afirmaciones es correcta</p>
	~<p>ente 1 y 2 Transformador 220V-12V CA<br></p>
}


// question: 9357  name: fuente switching a insertar
::fuente switching a insertar::[html]<p>Debe indicar el elemento faltante para completar la fuente</p><p><img src\="@@PLUGINFILE@@/diagramaEnBloques5-fala%20trafo.png" alt\="" role\="presentation"><br></p><p><br></p>{
	~<p>ente 2 y 3 Conmutador (oscilador 100khz)<br></p>
	~<p>ente 2 y 3 Rectificador 12V AC-DC<br></p>
	~<p>todas las opciones con afirmaciones son correctas</p>
	=<p>ninguna de las afirmaciones es correcta</p>
	~<p>ente 1 y 2 Rectificador 12V AC-DC<br></p>
}


// question: 9358  name: fuente switching a insertar.
::fuente switching a insertar.::[html]<p>Debe indicar el elemento faltante para completar la fuente</p><p><img src\="@@PLUGINFILE@@/diagramaEnBloques5-fala%20trafo.png" alt\="" role\="presentation"><br></p><p><br></p>{
	~<p>ente 1 y 2 Conmutador (oscilador 100khz)<br></p>
	~<p>ente 1 y 2 Rectificador 12V AC-DC<br></p>
	~<p>todas las opciones con afirmaciones son correctas</p>
	=<p>ninguna de las afirmaciones es correcta</p>
	~<p>ente 1 y 2 Rectificador 12V AC-DC<br></p>
}


// question: 9371  name: Inversor
::Inversor::[html]<p>Determinar cual de las siguientes afirmaciones es correcta considerando cada uno de los bloques.</p><p><img src\="@@PLUGINFILE@@/inversor.jpg" alt\="" role\="presentation"><br></p>{
	~<p>1- bateria de 12 V</p>
	~<p>2- oscilador a 50 Hz</p>
	~<p>3- transformador de 12V a 220V</p>
	=<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones son correctas</p>
}


// question: 9372  name: Inversor
::Inversor::[html]<p>Determinar cual de las siguientes afirmaciones es correcta considerando cada uno de los bloques.</p><p><img src\="@@PLUGINFILE@@/inversor.jpg" alt\="" role\="presentation"><br></p>{
	=<p>1- bateria de 12 V</p>
	~<p>1- oscilador a 50 Hz</p>
	~<p>1- transformador de 12V a 220V</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones son correctas</p>
}


// question: 9373  name: Inversor
::Inversor::[html]<p>Determinar cual de las siguientes afirmaciones es correcta considerando cada uno de los bloques.</p><p><img src\="@@PLUGINFILE@@/inversor.jpg" alt\="" role\="presentation"><br></p>{
	~<p>2- bateria de 12 V</p>
	=<p>2- oscilador a 50 Hz</p>
	~<p>2- transformador de 12V a 220V</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones son correctas</p>
}


// question: 9375  name: Inversor
::Inversor::[html]<p>Determinar cual de las siguientes afirmaciones es correcta considerando cada uno de los bloques.</p><p><img src\="@@PLUGINFILE@@/inversor.jpg" alt\="" role\="presentation"><br></p>{
	~<p>3- bateria de 12 V</p>
	~<p>1- oscilador a 50 Hz</p>
	~<p>2- transformador de 12V a 220V</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	=<p>ninguna de las respuestas con afirmaciones son correctas</p>
}


// question: 9374  name: Inversor (copia)
::Inversor (copia)::[html]<p>Determinar cual de las siguientes afirmaciones es correcta considerando cada uno de los bloques.</p><p><img src\="@@PLUGINFILE@@/inversor.jpg" alt\="" role\="presentation"><br></p>{
	~<p>3- bateria de 12 V</p>
	~<p>3- oscilador a 50 Hz</p>
	=<p>3- transformador de 12V a 220V</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuestas con afirmaciones son correctas</p>
}


// question: 9359  name: puente de Wien
::puente de Wien::[html]<p>Determinar cual de las siguientes afirmaciones es la correcta</p><p><img src\="@@PLUGINFILE@@/20210913_ExamenElect6to02.jpg" alt\="" role\="presentation"><br></p>{
	~<p>si R1\=R2\=R y C1\=C2\=C entonces F0\=1/(2.PI.R.C)</p>
	~<p>R3/R4 mayor que 2 es el factor de ganancia</p>
	~<p>El oscilador puente Wien nos permite obtener una señal senoidal</p>
	=<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuesta con afirmaciones es correcta</p>
}


// question: 9360  name: puente de Wien
::puente de Wien::[html]<p>Determinar cual de las siguientes afirmaciones es la correcta</p><p><img src\="@@PLUGINFILE@@/20210913_ExamenElect6to02.jpg" alt\="" role\="presentation"><br></p>{
	~<p>si R2\=R3\=R y C1\=C2\=C entonces F0\=1/(2.PI.R.C)</p>
	~<p>R1/R2 mayor que 2 es el factor de ganancia</p>
	~<p>El oscilador puente Wien nos permite obtener una señal cuadrada</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	=<p>ninguna de las respuesta con afirmaciones es correcta</p>
}


// question: 9361  name: puente de Wien
::puente de Wien::[html]<p>Determinar cual de las siguientes afirmaciones es la correcta</p><p><img src\="@@PLUGINFILE@@/20210913_ExamenElect6to02.jpg" alt\="" role\="presentation"><br></p>{
	=<p>si R1\=R1\=R y C1\=C2\=C entonces F0\=1/(2.PI.R.C)</p>
	~<p>R1/R2 mayor que 2 es el factor de ganancia</p>
	~<p>El oscilador puente Wien nos permite obtener una señal cuadrada</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuesta con afirmaciones es correcta</p>
}


// question: 9362  name: puente de Wien
::puente de Wien::[html]<p>Determinar cual de las siguientes afirmaciones es la correcta</p><p><img src\="@@PLUGINFILE@@/20210913_ExamenElect6to02.jpg" alt\="" role\="presentation"><br></p>{
	~<p>si R4\=R1\=R y C1\=C2\=C entonces F0\=1/(2.PI.R.C)</p>
	=<p>R3/R4 mayor que 2 es el factor de ganancia</p>
	~<p>El oscilador puente Wien nos permite obtener una señal cuadrada</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuesta con afirmaciones es correcta</p>
}


// question: 9363  name: puente de Wien
::puente de Wien::[html]<p>Determinar cual de las siguientes afirmaciones es la correcta</p><p><img src\="@@PLUGINFILE@@/20210913_ExamenElect6to02.jpg" alt\="" role\="presentation"><br></p>{
	~<p>si R4\=R1\=R y C1\=C2\=C entonces F0\=1/(2.PI.R.C)</p>
	~<p>R1/R4 mayor que 2 es el factor de ganancia</p>
	=<p>El oscilador puente Wien nos permite obtener una señal senoidal</p>
	~<p>todas las respuestas con afirmaciones son correctas</p>
	~<p>ninguna de las respuesta con afirmaciones es correcta</p>
}


